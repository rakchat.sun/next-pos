import React, { useState } from "react";
// Material-ui
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import BackspaceIcon from "@material-ui/icons/Backspace";
import CloseIcon from "@material-ui/icons/Close";
import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

// CSS
import "./Dialog.css";

// Router-dom
// import { useHistory } from "react-router-dom";

const useStyles = makeStyles({
  conPass: {
    padding: "0px",
    alignSelf: "center",
  },
  conOk: {
    justifyContent: "center",
  },
  rootClose: {
    padding: "0px",
    alignSelf: "flex-end",
    position: "absolute",
  },
  rootList: {
    padding: "0px",
    marginLeft: "auto",
    marginRight: "auto",
  },
  rootBtn: {
    background: "#fabd23",
    width: "96px",
    height: "40px",
    borderRadius: "20px",
    color: "black",
    fontWeight: "bold",
    textTransform: "none",
    marginBlock: "8px",
    fontSize: "24px",
    "&:hover": {
      background: "#fabd23",
    },
  },
  rootBtnOk: {
    background: "#000000",
    width: "224px",
    height: "56px",
    marginTop: "4px",
    marginBottom: "20px",
    borderRadius: "26px",
    color: "white",
    fontWeight: "bold",
    marginInline: "6px",
    fontSize: "24px",
    "&:hover": {
      background: "#000000",
    },
  },
  btnClose: {
    "&:hover": {
      background: "none",
    },
  },
  txtStar: {
    fontSize: "50px",
    paddingInline: "10px",
    color: "grey",
  },
  txtCLose: {
    marginTop: "2px",
    color: "grey",
  },
});

const responsive = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down("sm")]: {
      marginInline: "2px",
    },
    [theme.breakpoints.up("sm")]: {
      marginInline: "12px",
    },
  },
  txtHeader: {
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px",
      marginBottom: "0px",
      fontSize: "20px",
      fontWeight: "bold",
    },
    [theme.breakpoints.up("sm")]: {
      marginTop: "10px",
      marginBottom: "0px",
      fontSize: "22px",
      fontWeight: "bold",
    },
  },
}));

// Dialog PasswprdDialog
function ShowCancelDialog(props: any) {
  const classes = useStyles();
  const classesRes = responsive();
  // Props
  const { onClose, selectedValue, open, headerdilog, btndilog } = props;

  // Index ref value
  const [index, setIndex] = useState(0);
  // Star
  const [star, setStar] = useState(["*", "*", "*", "*", "*", "*"]);
  // Router go to pages
  // const history = useHistory();

  function handleAdd(n: string) {
    let num: string[] = star;
    if (index < 6) {
      num[index] = n;
      setStar(num);
      setIndex(index + 1);
    }
  }

  function handleDelete() {
    if (index >= 0) {
      let num: string[] = star;
      let indexStar: number = index - 1;
      num[indexStar] = "*";
      setStar(num);
      if (index !== 0) {
        setIndex(indexStar);
      }
    }
  }

  function handleOkPass() {
    var password = "";

    // Loop variable
    for (let i: number = 0; i < star.length; i++) {
      password += `${star[i]}`;
    }

    // Check password
    // Comfirm cancel order dialog
    if (headerdilog === "Confirm Cancel Order") {
      if (password === "999999") {
        // history.push("/closescreen");
        onClose(selectedValue);
        let starClear = ["*", "*", "*", "*", "*", "*"];
        setStar(starClear);
        setIndex(0);
      } else {
        let starClear = ["*", "*", "*", "*", "*", "*"];
        setStar(starClear);
        setIndex(0);
      }
    }
  }

  // Close dialog
  function handleClose() {
    onClose(selectedValue);
    let starClear = ["*", "*", "*", "*", "*", "*"];
    setStar(starClear);
    setIndex(0);
  }

  return (
    <Dialog
      PaperProps={{
        style: { borderRadius: "26px" },
      }}
      onClose={handleClose}
      open={open}
      maxWidth={"xs"}
      fullWidth={true}
    >
      <DialogTitle className={classes.conPass}>
        <Typography className={classesRes.txtHeader}>{headerdilog}</Typography>
      </DialogTitle>
      <DialogTitle className={classes.rootClose} onClick={() => handleClose()}>
        <Button disableRipple className={classes.btnClose}>
          <CloseIcon fontSize="large" classes={{ root: classes.txtCLose }} />
        </Button>
      </DialogTitle>
      {/* mt 1 = 8px */}
      <Box mt={0.5} display="flex" flexDirection="row" justifyContent="center">
        <Typography className={classes.txtStar}>{star[0]}</Typography>
        <Typography className={classes.txtStar}>{star[1]}</Typography>
        <Typography className={classes.txtStar}>{star[2]}</Typography>
        <Typography className={classes.txtStar}>{star[3]}</Typography>
        <Typography className={classes.txtStar}>{star[4]}</Typography>
        <Typography className={classes.txtStar}>{star[5]}</Typography>
      </Box>
      <Grid container justifyContent="center">
        <Grid item xs={4} style={{ textAlign: "end" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("7")}
            className={classes.rootBtn}
          >
            7
          </Button>
        </Grid>
        <Box className={classesRes.root}>
          <Grid item style={{ textAlign: "center" }}>
            <Button
              disableRipple
              onClick={() => handleAdd("8")}
              className={classes.rootBtn}
            >
              8
            </Button>
          </Grid>
        </Box>
        <Grid item xs={4} style={{ textAlign: "start" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("9")}
            className={classes.rootBtn}
          >
            9
          </Button>
        </Grid>
        <Grid item xs={4} style={{ textAlign: "end" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("4")}
            className={classes.rootBtn}
          >
            4
          </Button>
        </Grid>
        <Box className={classesRes.root}>
          <Grid item style={{ textAlign: "center" }}>
            <Button
              disableRipple
              onClick={() => handleAdd("5")}
              className={classes.rootBtn}
            >
              5
            </Button>
          </Grid>
        </Box>
        <Grid item xs={4} style={{ textAlign: "start" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("6")}
            className={classes.rootBtn}
          >
            6
          </Button>
        </Grid>
        <Grid item xs={4} style={{ textAlign: "end" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("1")}
            className={classes.rootBtn}
          >
            1
          </Button>
        </Grid>
        <Box className={classesRes.root}>
          <Grid item style={{ textAlign: "center" }}>
            <Button
              disableRipple
              onClick={() => handleAdd("2")}
              className={classes.rootBtn}
            >
              2
            </Button>
          </Grid>
        </Box>
        <Grid item xs={4} style={{ textAlign: "start" }}>
          <Button
            disableRipple
            onClick={() => handleAdd("3")}
            className={classes.rootBtn}
          >
            3
          </Button>
        </Grid>
        <Grid item xs={4} style={{ textAlign: "end" }}></Grid>
        <Box className={classesRes.root}>
          <Grid item style={{ textAlign: "center" }}>
            <Button
              disableRipple
              onClick={() => handleAdd("0")}
              className={classes.rootBtn}
            >
              0
            </Button>
          </Grid>
        </Box>
        <Grid item xs={4} style={{ textAlign: "start" }}>
          <Button
            disableRipple
            onClick={() => handleDelete()}
            className={classes.rootBtn}
          >
            <BackspaceIcon
              style={{ color: "rgba(0, 0, 0, 0.54)" }}
            ></BackspaceIcon>
          </Button>
        </Grid>
        <Grid style={{ marginBlock: "8px" }} classes={{ root: classes.conOk }}>
          <Button
            disableRipple
            onClick={() => handleOkPass()}
            className={classes.rootBtnOk}
          >
            {btndilog}
          </Button>
        </Grid>
      </Grid>
    </Dialog>
  );
}

export default function DialogConfirmCancel() {
  const [open, setOpen] = useState(false);
  const [headerdilog, setHeaderDialog] = useState("");
  const [btndilog, setBtnDialog] = useState("");

  // Open dialog
  function handleClickOpen(txt: string, btn: string) {
    setHeaderDialog(txt);
    setBtnDialog(btn);
    setOpen(true);
  }

  // Close dialog
  function handleClose() {
    setOpen(false);
  }

  return (
    <Box>
      <Button
        variant="contained"
        color="secondary"
        onClick={() => handleClickOpen("Confirm Cancel Order", "Confirm")}
      >
        Confirm Cancel Order
      </Button>

      <ShowCancelDialog
        open={open}
        onClose={handleClose}
        headerdilog={headerdilog}
        btndilog={btndilog}
      />
    </Box>
  );
}
