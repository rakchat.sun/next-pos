import React from "react";
import Link from "next/link";
import Image from "next/image";
import styles from "../styles/Home.module.css";

export default function Home() {
  let number: number = 1;
  React.useEffect(() => {
    // alert(number);
  });

  return (
    <div>
      <h1>Home Screen</h1>
      <Link href="/">
        <a>Go to Index</a>
      </Link>
    </div>
  );
}
