import React from "react";
import Link from "next/link";

export default function about() {
  return (
    <div>
      <h1>About Screen</h1>
      <Link href="/">
        <a>Go to Index</a>
      </Link>
    </div>
  );
}
