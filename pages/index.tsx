import React from "react";
import Link from "next/link";
import Image from "next/image";
import styles from "../styles/Home.module.css";

// Screen
import Sun from "../components/Sun";
import DialogPassword from "../components/DialogPassword";

export default function Home() {
  let number: number = 1;
  React.useEffect(() => {
    // alert(number);
  });

  return (
    <div className={styles.container}>
      <h1>Index Screen</h1>
      <Sun name="Suntorn" lname="Rakchat" />
      <li>
        <Link href="/home">
          <a>Go to Home</a>
        </Link>
      </li>
      <li>
        <Link href="/about">
          <a>Go to About</a>
        </Link>
      </li>
      <li>
        <Link href="/dialog">
          <a>Go to Dialog</a>
        </Link>
      </li>
    </div>
  );
}
