import React, { useEffect } from "react";
import Link from "next/link";
import Image from "next/image";
import styles from "../styles/Home.module.css";

import { useRouter } from "next/router";

// Screen
import Sun from "../components/Sun";
import DialogPassword from "../components/DialogPassword";

// Password Screen
function PasswordScreen() {
  return (
    <div>
      <h1>Password Screen</h1>
    </div>
  );
}

// Confirm Open Screen
function OpenScreen() {
  return (
    <div>
      <h1>Confirm Open Order Screen</h1>
    </div>
  );
}

// Confirm Close Screen
function CloseScreen() {
  return (
    <div>
      <h1>Confirm Cancel Order Screen</h1>
    </div>
  );
}

export default function dialog() {
  return (
    <div>
      <h1>Dialog Screen</h1>
      <Link href="/">
        <a>Go to Index</a>
      </Link>
      <br />
      <br />
      <br />
      <DialogPassword />
    </div>
  );
}
